#3D DESIGN

#TINKERCAD

To create a 3D digital design you go to Tinkercad

-adding shapes
-Moving shapes
-Stretch up/down
-Make holes
-Resize shape

1. First you click on 3D design and then "create new design"

![Screenshot](../img/tink1.jpg)

2. You can use all kinds of shapes to design

![Screenshot](../img/tink2.jpg)

3. On the left you can either chose to copy, paste and delete your shape/design
You can also look at your design from every perspective

![Screenshot](../img/tink3.jpg)

4. When you want to change the size of your design you click on it and change it

![Screenshot](../img/tink4.jpg)

5. When you're finished you export your design as an stl file

![Screenshot](../img/tink5.jpg)

![Screenshot](../img/tink6.jpg)

#Inkscape

Inkscape is a free and open-source vector graphics editor. This software can be used to create or edit
vector graphics such as illustrations, diagrams, line arts, charts, logos, business cards, book covers,
icons, CD/DVD covers, and complex paintings.

#Fusion 360

Fusion 360 is available for free personal use for individuals who are:

Using for personal projects outside of their primary employment.
Engaged in hobby businesses.*
Learning for personal use, outside of a company environment or commercial training.
Creating YouTube videos, blogs or other web content.

After downloading Fusion 360 we got to know the program by checking out all the shortcuts for drawing your own 3D Design.
it didn't differ to much  from the tinkercad but you got more options in Fusion 360. The environment was more specific, more options.

-C for Circles
-R for Rectangles
-L for a Line
-E for Extrude
-H for Hole
-M for Move

1.select a pan you want to work on then click r for rectangle and a rectangle will appear

![Screenshot](../img/fusion1.jpg)

2.select e wich will extrude your design you can decide how much you want to extrude

![Screenshot](../img/fusion2.jpg)

3.type H for hole select a face point and decide how deep you want your hole to be you can drag the end or type it in the hole control section

![Screenshot](../img/fusion3.jpg)

4.to export a file go to file click on export and export it as an stl fil

![Screenshot](../img/fusion4.jpg)

![Screenshot](../img/fusion5.jpg)
