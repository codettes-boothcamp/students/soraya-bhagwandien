#Embedded Programming

Week3

#Introduction to Arduino Starter kit-Components

Arduino Uno is the microcontroller development board that will be at the heart of your projects. It's a simple computer, but one that has no way for you to interact with it yet. You will be building the circuits and interfaces for interaction, and telling the microcontroller how to interface with other components.

![Screenshot](../img/emd10.jpg)

This is a Breadboard - A board on wich you can buil electronic circuits. It's like a patch panel with rows of holes that allow you to connect wires and components together. Versions that require soldering are available as well as the solder-less type used here.

![Screenshot](../img/emd7.jpg)

This is resistors - Resist the flow of electrical energy in a circuit, changing the voltage and current as a result. Resistor values are measured in ohms (represented by the Greek omega character: ). The colored strips on the sides of resistors indicate their value (see resistor color code table).

![Screenshot](../img/emd11.jpg)

This a pushbutton - Momentary switches that close a circuit when pressed. They snap into bread-board easly. These are good for detecting on/off signals.
![Screenshot](../img/emd12.jpg)

#Steps how to code on Arduino Uno 

STEP1. we learn how to install Arduino

![Screenshot](../img/emd1.jpg)

#Blink (Sketch)+ hero kit

Upload Blink Sketch
Blink Built in Led 12 and 13

STEP2. Navigate to the LED blink example skecth ('sketch' is what Arduino programs are called). It's located under:
FILE>EXAMPLES>01.BASICS>BLINK 

![Screenshot](../img/emd2.jpg)
 
STEP3. A window with some text in it should have opened. Leave the window be for now, and select your boardunder:
TOOLS>BOARD menu

![Screenshot](../img/emd3.jpg)

STEP4. Choose the serial port your Arduino is connected to from the 
TOOLS>SERIAL PORT menu.

![Screenshot](../img/emd4.jpg)

STEP5. To upload the Blink sketch to your Arduino, press the UPLOAD toggle in the top left corner of the window. See Fig.1.

![Screenshot](../img/emd5.jpg)

STEP6. You should see a bar indicating the progress of the upload near the lower left corner of the Arduino IDE, and the lights labeled TX and RX on the Arduino board will be blinking. If the upload is successful the IDE will display the message DONE UPLOADING.

STEP7. A few seconds after the upload has completed, you should see the yellow LED with an L next to it start blinking. See Fig.2.
If this is the case, congratulations! You've succesfully programmed the Arduino to blink its onboard LED!

![Screenshot](../img/emd6.jpg)

#Spaceship Interface

In the void setup I have pin on pinMode LED 12 INPUT and 13 as OUTPUT. 
And in the loop I give permission on digitalWrite to LED 12 for one second high and one second low and for LED 13 the same.

```
/*
  Blink

  Turns an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products

  modified 8 May 2014
  by Scott Fitzgerald
  modified 2 Sep 2016
  by Arturo Guadalupi
  modified 8 Sep 2016
  by Colby Newman

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Blink
*/
int switchState = 0;

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(12,INPUT);
  pinMode(13,OUTPUT);
 
}

// the loop function runs over and over again forever
void loop() {
  switchState = digitalRead(2);
  if (switchState == LOW) {
  digitalWrite(13, HIGH);
  digitalWrite(12, LOW);

}else {
  digitalWrite(13, LOW);
  digitalWrite(12, HIGH);
}

  delay(500); 
  digitalWrite(13, HIGH);
  digitalWrite(12, LOW);
  delay(500);

}

```

#Running lights

LED 12 and 13.
 
![Screenshot](../img/emd8.jpg)

#Procedural Programming
```
void setup() {
// setup your hardware
	...
}

void subprocedure() {
// switch off all leds
...
}

void loop() {
// do ain loop stuff
subprocedure(); //call subroutine
// switch only the appropriate led on
	...
} //end

void 2maal( int x )
{
 //x = the vaule passed from the function call
}


void loop()
{
MyFunction(1000);
}

EXAMPLE

void setup() {
// setup your hardware
}
void ledsOff() {
//switch all leds off
digitalwrite(12,0);
digitalwrite(13,0);
}
void runningLeds(){
	ledsOff();
	digitalwrite(12,1);
delay(250);
	ledsOff();
	digitalwrite(13,1);
delay(250);
}

void loop() {
	switchState = digitalread(2);
	if(switchState==0){
	ledsOff();
	digitalwrite(12,HIGH);
}else{
	runningLeds();
}

```

#Week4 

#Serial Communication

In the void setup we begin the serial monitor at a rate of 9600.
In the void loop we are using the Serial.println function and what we are printing is the value found in A0 by the Analogread.

```
void setup() {
  // setup your serial
  Serial.begin(9600);
  Serial.println('hello world');
  
}

void loop() {
  // Send a message to your serial port/monitor
  Serial.println("change");
  delay(2000);

}

```

#LDR

We used the serial communication to turn off and on a LED.
We created a integer to store the incoming serial byte.
In the void setup, we set pin 3,4 & 5 as an output and use "Serial.begin(9600)" to initiate the serial communication.
We used 9600 bits per second.
Then we created a integer to run all the leds off and we called it "ledsOff" and to turn the leds on one by one
we created "RunningLeds". We open the serial monitor, which prints different values on the screen.
A potentiometer goes from max and min values of 0 to 1023. Put a black paper on the resistor to prevent light from falling
on it and that's the minimum value. Put a torch on the resistor and that's the maximum value. We want to read the percent
so we craeted a formula.
Note* The 100L is important in order to avoid overflowing a 16 bit intergar calculation.

```
//parameter
int minL = 200;
int maxL = 1023;
int lightInt = 0;
int lightPc = 0; 

void setup() {
     pinMode(3,OUTPUT);
     pinMode(4,OUTPUT);
     pinMode(5,OUTPUT);
     
     Serial.begin(9600);
     Serial.println("hallo");
}
// Subroutines
void ledsOff(){
  digitalWrite(3,0);
  digitalWrite(4,0);
  digitalWrite(5,0);
}

void runningLeds(){
  ledsOff();
  digitalWrite(3,1);
  delay(250);
  ledsOff();
  digitalWrite(4,1);
  delay(250);
  ledsOff();
  digitalWrite(5,1);
  delay(250);
}

void loop(){
     lightInt = analogRead(A0);
     //Serial.println(lightInt);
     lightPc = (lightInt-minL)*100L/(maxL-minL);
     Serial.println (lightPc);
     
     
    ledsOff(); 
    if(lightPc>=30){digitalWrite(3,1);};
    if(lightPc>=60){digitalWrite(4,1);};
    if(lightPc>=90){digitalWrite(5,1);};
    if(lightPc> 95){runningLeds();};
    delay(1000);
}
```

#PWM

Pulse Width Modulation, or PWM, is a technique for getting analog results with digital means. Digital control is used
to create a square wave, a signal switched between on and off. So if we want to dim a LED, we can change the on and off time of the signal.
If we will change the on and off time fast enough then the brightness of the led will be changed.
And that we're doing with the "dutyCycle". DutyCycle is the percentage of time when the signal was high during the time of period.
So at 10% dutyCycle, the led will be high for 10% and will be low for 90% (100-dutyCycle).
Now we want to dim de led with cycleTime. If the cycleTime is lower, the led will go faster on and off.
We set the cylceTime on 50.
The PWM signal can give the value from 0-255. We created the "potValue" on pin 5.

```
//parameter
int minL = 200;
int maxL = 1023;
int lightInt = 0;
int lightPc = 0; 
int potValue = 0;

void setup() {
     pinMode(3,OUTPUT);
     pinMode(4,OUTPUT);
     pinMode(5,OUTPUT);
     
     Serial.begin(9600);
     Serial.println("hallo");
}
// Subroutines
void ledsOff(){
  digitalWrite(3,0);
  digitalWrite(4,0);
  digitalWrite(5,0);
}

void runningLeds(){
  ledsOff();
  digitalWrite(3,1);
  delay(250);
  ledsOff();
  digitalWrite(4,1);
  delay(250);
  ledsOff();
  digitalWrite(5,1);
  delay(250);
}
void dimled(int ledPin = 3, int dutyCycle = 10){
  int cycleTime = 50;
  digitalWrite (ledPin,1);
  delay (dutyCycle*cycleTime/100);
  digitalWrite (ledPin,0);
  delay ((100 - dutyCycle)*cycleTime/100);
  
}

void loop(){
  potValue = analogRead (A0);
  Serial.println(potValue);
  
 analogWrite(5,potValue/4);
  
//   for (int i =0; i <=255; i++){
//       analogWrite (3, i);
       delay (500);
//}

}
```

#DHT11
The DHT11 is a digital-output relative humidity and temperature sensor.

-The sensor looks like this

![Screenshot](../img/emd13.jpg)

My sensor has 3 pins and it's fitted on a board.
Use 3 wires to connect it to the Arduino. The DHT11 senor use just one signal wire to transmit data to the Arduino. The three pin
DHT11 sensor already have a 10 kohm pull up resistor.

![Screenshot](../img/emd14.jpg)

-You need to add the library to the Arduino.

In the Arduino, go to Sketch >> Include Library >> Add ZIP file. When you click the 'Add .ZIP library', you should get a file window that pops up. Add the DHT_Library.zip.

![Screenshot](../img/emd15.jpg)

#Upload the code.
```
// Example testing sketch for various DHT humidity/temperature sensors
// Written by ladyada, public domain

// REQUIRES the following Arduino libraries:
// - DHT Sensor Library: https://github.com/adafruit/DHT-sensor-library
// - Adafruit Unified Sensor Lib: https://github.com/adafruit/Adafruit_Sensor

#include "DHT.h"

#define DHTPIN 4    // Digital pin connected to the DHT sensor
// Feather HUZZAH ESP8266 note: use pins 3, 4, 5, 12, 13 or 14
// Pin 15 can work but DHT must be disconnected during program upload.

// Uncomment whatever type you're using!
#define DHTTYPE DHT11   // DHT 11
//#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

// Connect pin 1 (on the left) of the sensor to +5V
// NOTE: If using a board with 3.3V logic like an Arduino Due connect pin 1
// to 3.3V instead of 5V!
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

// Initialize DHT sensor.
// Note that older versions of this library took an optional third parameter to
// tweak the timings for faster processors.  This parameter is no longer needed
// as the current DHT reading algorithm adjusts itself to work on faster procs.
DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(9600);
  Serial.println(F("DHTxx test!"));

  dht.begin();
}

void loop() {
  // Wait a few seconds between measurements.
  delay(2000);

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t) || isnan(f)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }

  // Compute heat index in Fahrenheit (the default)
  float hif = dht.computeHeatIndex(f, h);
  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);

  Serial.print(F("Humidity: "));
  Serial.print(h);
  Serial.print(F("%  Temperature: "));
  Serial.print(t);
  Serial.print(F("°C "));
  Serial.print(f);
  Serial.print(F("°F  Heat index: "));
  Serial.print(hic);
  Serial.print(F("°C "));
  Serial.print(hif);
  Serial.println(F("°F"));
}
```

-When the code is uploaded, open the Serial Monitor.
-You will see the humidity and temperature.

#Tinkercad

Tinkercad is a free online collection of software tools that help people all over the world think, create and make.
We’re the ideal introduction to Autodesk, the leader in 3D design, engineering and entertainment software.

-Make an accout
-After making an account go to circuits and create a new circuit

![Screenshot](../img/emd16.jpg)

-We learned how to control DC motors using Arduino.
-First you have to build the circuit

![Screenshot](../img/emd17.jpg)

-We can control the speed of the DC motor by simply controlling the input voltage to the motor and
the most common method of doing that is by using PWM signal.

![Screenshot](../img/emd18.jpg)

-PWM, or pulse width modulation is a technique which allows us
to adjust the average value of the voltage that’s going to the electronic device by turning on and off the power
at a fast rate.

#Here is the code
```
// Declare ur varables
const int pvm = 3;
const int in_1 = 8;
const int in_2 = 9;


void setup(){
  pinMode(in_1, OUTPUT);
  pinMode(in_2, OUTPUT);
  pinMode(3, OUTPUT);
}

void loop(){
  analogWrite(pvm,64));
    // turn CW
  digitalWrite(in_1, LOW);
  digitalWrite(in_2, HIGH);
  delay(3000);
  }
    //BRAKE
  digitalWrite(in_1, HIGH);
  digitalWrite(in_2, HIGH);
  delay(1000);
  }
   // turn CCW
  digitalWrite(in_1, HIGH);
  digitalWrite(in_2, LOW);
  delay(3000);
  }
    // BRAKE
  digitalWrite(in_1, HIGH);
  digitalWrite(in_2, HIGH);
  delay(1000);
  }
 
```

#H-Bridge DC Motor Control

On the other hand, for controlling the rotation direction,
we just need to inverse the direction of the current flow through the motor,
and the most common method of doing that is by using an H-Bridge.
So if we combine these two methods, the PWM and the H-Bridge, we can have a complete control over the DC motor.

![Screenshot](../img/emd19.jpg)

-Jogg button control
(Wiring speed & direction Control. Add a potentiometer and use A0 as the speedPin)

![Screenshot](../img/emd20.jpg)

#Wiring the speed control

-Upload the code and open the serial monitor

![Screenshot](../img/emd21.jpg)

```
#Here is the code

// Declare ur varables
const int pvm = 3;
const int in_1 = 8;
const int in_2 = 9;


void setup(){
  pinMode(in_1, OUTPUT);
  pinMode(in_2, OUTPUT);
  pinMode(3, OUTPUT);
  Serial.begin(9600);
}

void loop(){
  int duty = analogRead(A0)-512/2;
  Serial.println(duty);
  analogWrite(pvm,abs(duty));
  if (duty>0){
    // turn CW
  digitalWrite(in_1, LOW);
  digitalWrite(in_2, HIGH);
  }
  if(duty<0){
    // turn CW
  digitalWrite(in_1, HIGH);
  digitalWrite(in_2, LOW);
  }
  if(duty==0){  
    // BRAKE
  digitalWrite(in_1, HIGH);
  digitalWrite(in_2, HIGH);
  }
}

```
Mapping function

The code for the mapping function
```
// Declare ur variables
const int pwm = 3;
Const int jogPin =A0; // connects the pot-meter
const int in_1 = 8;
const int in_2 = 9;
Const int deadZone-5; 	//after starts repond
Int duty=0; 		// duty cycle for PWM


void setup(){
	pinMode(in_1, OUTPUT);	
  	pinMode(in_2, OUTPUT);
  	pinMode(3, OUTPUT);
  	Serial.begin(9600);
}

void loop(){
 duty = map(analogRead(jogPin),0,1023,-255,255);
  	Serial.println(duty);
  	analogWrite(pwm,abs(duty)); // abs duty to PWM
  	if(duty> 0 - deadZone){
        // turn CW
      digitalWrite(in_1,LOW);
      digitalWrite(in_2,HIGH); 
    }
  	if(duty < 0 - deadZone){
        // turn CCW
      digitalWrite(in_1,HIGH);
      digitalWrite(in_2,LOW); 
    }
  	if(abs(duty) <= deadZone){
        // BRAKE
      digitalWrite(in_1,HIGH);
      digitalWrite(in_2,HIGH); 
    }  
}

```



