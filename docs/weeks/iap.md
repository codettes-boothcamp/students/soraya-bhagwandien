#INTERFACE

The Raspberry Pi is a low cost, credit-card sized computer that plugs into a computer monitor or TV, and uses a standard keyboard and mouse.
It is a capable little device that enables people of all ages to explore computing, and to learn how to program in languages like Scratch and Python.
It’s capable of doing everything you’d expect a desktop computer to do, from browsing the internet and playing high-definition video,
to making spreadsheets, word-processing, and playing games.

#How to find my Laptop Adress

Search on your laptop "Command Prompt"

Type: ipconfig

![Screenshot](../img/iap1.jpg)

#TOOLS

-Raspberry Pi
-Ethernet or Cross Cables
-Sd Card 16gb min
-Power Cable micro usb
-Laptop ethernet slot

![Screenshot](../img/iap2.jpg)

#BASIC RASPBERRY PI SETUP

1.Download Raspbian version (Jessie or Stretch)
2.Install Disk32Imager
3.Install Putty
4.Install Winscp
5.Install Notepad++6.
6.Install Angry ip scanner

#NOTE: I used Raspberry Pi 3 so Raspbian Jessie.

#Insert SD card in laptop

Write Raspbian Jessie on SD card using Win32DiskImager

-Select Raspberry Pi on SD and add an empty file "ssh"

![Screenshot](../img/iap3.png)

-Edit the file "cmdline.txt" on Notepad++

![Screenshot](../img/iap4.png)

-Type in IP adress "192.168.1.152"
Save the file and move/insert SD card into Raspberry Pi and connect to that IP adress

-Open Putty and type in the IP adress, select "SSH", give it a name and save

![Screenshot](../img/iap5.jpg)

-Login

-User: pi

-Password: raspberry

-Type in: sudo raspi-config

![Screenshot](../img/iap6.jpg)

-Configuration

Up down left right keys on keyboard to navigate

![Screenshot](../img/iap7.jpg)

#Install node.js

Bootstrapping Nodejs

With PuTTY you have create folder name project1. Command cd nodjes/ cd projects/ cd project1.
Standing in the directory project1 you command npm init -y (by doing this you initializ the npm package.json)
Than command npm i D nodemon ()
Also command npm i express (to install nodejs express app)

Create in folder project1 an file. Command touch index.js

Node.js came into existence when the original developers of JavaScript extended it from something you could only run in the browser to something you could run on your machine as a standalone application.

Now you can do much more with JavaScript than just making websites interactive.

JavaScript now has the capability to do things that other scripting languages like Python can do.

Something that has happened in our app that we can respond to. There are two types of events in Node.

-System Events: C++ core from a library called libuv. (For example, finished reading a file).

-Custom Events: JavaScript core.

#Python

Python is an oldie, but a goodie. This programming language originated in the early ’90s and is still one of the most innovative, flexible, and versatile technologies thanks to its continually developing libraries, excellent documentation, and cutting-edge implementations.

Python also has one of the largest communities that contributes to improving the language to handle modern-day programming tasks, as shown in this diagram.

#Node.js server + Express app

First create a directory named myapp, change to it and run npm init. Then install express as a dependency, as per the installation guide.

In the myapp directory, create a file named app.js and copy in the code from the example above.

install in the root => pi@raspberrypi:~ $ node app.js

Then, load http://localhost:192.168.1.152:5000 in a browser to see the output.

#Python server + Flask app

-Build a Flask website
You’re going to set up a basic web application with Flask and Python.

-If you don’t already have Python 3 on your computer you will need to download and install it.


-Download Python 3 and install it.

-How to install Python 3
You will also need to install the Flask package.


-Install the flask Python module using pip. Make sure you are connected to the internet before you start.

-Installing Python modules with pip
Once Flask is installed, you can create your web application.


-Open a terminal or command prompt window, and make a new directory called webapp for your project.

-mkdir webapp

-Use the change directory command cd to open the new directory.

-cd webapp

-Open Python 3 IDLE.


-Create a new file by clicking File and then New file, and save it as app.py inside the webapp folder you just created.


-Now enter the following lines of code into the blank app.py window:

```
from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return 'Hello world'

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')

```

```
You will explore this code in more detail in the next step, but for now let’s keep it simple and make sure everything works.


Save your changes by clicking File and then Save, or by pressing Ctrl and S.

You will need to run your web app from the terminal/command prompt window you opened earlier.

On Raspberry Pi/Linux/macOS
Enter the command python3 app.py into the terminal window.

On Windows
Enter the command python app.py into the command prompt window.

If everything has been set up correctly, you should see an output similar to this:

```

```
* Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
* Restarting with stat
* Debugger is active!
* Debugger pin code: ***-***-***

```

```
pi run web app


Open your web browser and enter the URL http://192.168.1.152:5000/. You should see a white screen with the words Hello world.

Note: 192.168.1.152 means ‘home’, i.e. this computer. :5000 means ‘port 5000’, which is the port the web server is running on.

Flask "Hello world"

#Serial communication

The RaspberryPi send data to Arduino uno

```
import serial

import time

#Define the serial port and baud rate.

#Ensure the 'COM#' corresponds to what was seen in the Windows Device Manager

```
ser = serial.Serial('/dev/ttyACM0', 9600)   # ttyACM0 serial port for pi 2 #/dev/ttyAMA0 pi 3
def led_on_off():
    user_input = input("\n Type on / off / quit : ")
    if user_input =="on":
        print("LED is on...")
        time.sleep(0.1) 
        ser.write(b'H') 
        led_on_off()
    elif user_input =="off":
        print("LED is off...")
        time.sleep(0.1)
        ser.write(b'L')
        led_on_off()
    elif user_input =="quit" or user_input == "q":
        print("Program Exiting")
        time.sleep(0.1)
        ser.write(b'L')
        ser.close()
    else:
        print("Invalid input. Type on / off / quit.")
        led_on_off()

time.sleep(2) # wait for the serial connection to initialize

led_on_off()

```

![Screenshot](../img/iap8.jpg)

#HTML

![Screenshot](../img/iap9.jpg)

HTML5 is the latest evolution of the standard that defines HTML. The term represents two different concepts. It is a new version of the language HTML, with new elements, attributes, and behaviors, and a larger set of technologies that allows the building of more diverse and powerful Web sites and applications. This set is sometimes called HTML5 & friends and often shortened to just HTML5.
Designed to be usable by all Open Web developers, this reference page links to numerous resources about HTML5 technologies, classified into several groups based on their function.

1.Semantics: allowing you to describe more precisely what your content is.
2.Connectivity: allowing you to communicate with the server in new and innovative ways.
3.Offline and storage: allowing webpages to store data on the client-side locally and operate offline more efficiently.
4.Multimedia: making video and audio first-class citizens in the Open Web.
5.2D/3D graphics and effects: allowing a much more diverse range of presentation options.
6.Performance and integration: providing greater speed optimization and better usage of computer hardware.
7.Device access: allowing for the usage of various input and output devices.
8.Styling: letting authors write more sophisticated themes.

HTML is a scripting language, (Instruction tag)
Beginn with <> and closed by </>

NEW Semantic TAGS that actually have more meaning to the standard tags for layout.
For layout :,,,,  
For widgets: ,  etc
Working with HTML5 we have a certain structure:
The root folder is the public folder

1.Public
a. index.html
b. img
c. css
d. fonts
e. js
- lib

HTML5 websites have the same structure:

1.Site Map
2.Folder Structure
3.Collect Context
4.Look & Feel/Layout
5.Mock up
6.Target Devices

CSS is for the look and style
CSS3 or  Cascading Style Sheets enables the separation of presentation and content, including layout, colors, and fonts. To improve content accessibility, more flexibility and control of presentation characteristics, web pages share formatting by specifying the relevant CSS in a separate .css file

#Websocket
The WebSocket API is an advanced technology that makes it possible to open a two-way interactive communication session between the user's browser and a server.
With this API, you can send messages to a server and receive event-driven responses without having to poll the server for a reply.

Interfaces

1.WebSocket: The primary interface for connecting to a WebSocket server and then sending and receiving data on the connection.
2.CloseEvent: The event sent by the WebSocket object when the connection closes.
3.MessageEvent: The event sent by the WebSocket object when a message is received from the server.

examples:

```
<Head>
	<script> websocket lib.js </script> //setup connection to main socket server
	<script></script>
</Head>

```

Using libaries:

1.aan/uit knop
2.Chart
3.Chat (websocket)
we have all kinds of libaries, if we are using them we got to link them to the css file like this:
lib:

```
<script............/>
or
<script> console.log(Hello World!)</script>

<Body> 
	chart.show()
</Body>

```

For visual libary:nice buttons use the bootstrap.lib JQuery.lib

For infra libary use websocket.js

For utilities use also the JQuery libary

#JavaScript

JavaScript was initially created to “make web pages alive”. With NodeJS now also server-side or on any device that has a special program called the JavaScript engine. It's a Functional Programming style.

What can it do?

1.Add new HTML to the page, change the existing content, modify styles.
2.React to user actions, run on mouse clicks, pointer movements, key presses.
3.Send requests over the network to remote servers, download and upload files (so-called AJAX and COMET technologies).
4.Get and set cookies, ask questions to the visitor, show messages.
5.Remember the data on the client-side (“local storage”)

What can’t it do?

1.Directly talk to your computer or device functions
2.Is NOT allowed to communicate freely between sites
3.One browser window does NOT know of any other browser window/tab
4.Cross domain communication crippled
5.Be compiled so all code is visible to others
6.Is considered insecure

#Using Objects and Arrays:

```
Object={
		}
		
And 

Array = [ , , ]

GetElementByTag

= wordt
== equal to
+= was and new

examples:
Array: 
var mygifs[ ];
mygifs=["car","boat","bff"]
if we choose to have "bff" then we write: mygifs(2);

mygifs.shift (adds a value)

We array: 
1. shift
2. push
3. pop

Object:
var myObj{};
value pairs
	={temp:"10",
		highTemp:[, ,],
		onclick:","
	}
	
```

#Interconnect NodeJs to MQTT

On my Rpi nodejs/project folder we ticked the following in the console:
> sudo npm install --save mqtt
We edit the index.js and add the followiing code:

```
var mqtt  = require('mqtt');
var client  = mqtt.connect('mqtt://127.0.0.1');

client.on('connect', function () {
client.subscribe('#');
client.publish('/', 'Connected to MQTT-Server');
console.log("\nNodeJS Connected to MQTT-Server\n");
});
// send all messages from MQTT to the Websocket with MQTT topic
client.on('message', function(topic, message){
console.log(topic+'='+message);
io.sockets.emit('mqtt',{'topic':String(topic),payload':String(message)});
});

```

#Interconnect WebSockets with MQTT

I add the reverse from socket back to MQTT inside the SocketIO code:

```
io.sockets.on('connection', function (socket) {

// Add to handle from Socket to MQTT:
socket.on('mqtt', function (data) {
console.log('Receiving for MQTT '+ data.topic + data.payload);
 	// TODO sanity check .. is it valid topic ... check ifchannel is "mq$
client.publish(data.topic, data.payload);
});

```

#Socket connect, subscribe,store data

1.Socket connect
2.Subscribe to topic
3.onReceipt of MQTT topic messages

```
 <script type="text/javascript">
      var socket = io.connect(window.location.hostname + ":5000");
      socket.on('connect', function() {
        socket.emit('subscribe', {
              topic: '/ESP/#'
          });

          socket.on('mqtt', function(msg) {
              var msgTopic = msg.topic.split("/");
              var topic = msgTopic[3];
              var id = msgTopic[2];
              console.log(msg.topic + ' ' + msg.payload);
              //console.log('#'+topic.concat(id));
              // Added for storage of all incoming MQTT messages in Array
              storeMQData(topic.concat(id), msg.payload);
              // end insert
              $('#' + topic.concat(id)).html(msg.payload);
          });
 //$('input.cb-value').prop("checked", true);
          $('.cb-value').click(function() {
              var mainParent = $(this).parent('.toggle-btn');
              if ($(mainParent).find('input.cb-value').is(':checked')) {
                  $(mainParent).addClass('active');
                  socket.emit('control', {
                      'topic': "/ESP/" + $(this).parents('div').attr('id'),
                      'payload': "on"
                  });
              } else {
                  $(mainParent).removeClass('active');
                  socket.emit('control', {
                      'topic': "/ESP/" + $(this).parents('div').attr('id'),
                      'payload': "off"
                  });
                  //io.sockets.emit('mqtt',"/ESP/unit2/pump=off");
                  //socket.emit('closecmd', id);
              }
          });
      });
  </script>

```

#Create a Web Widget

Web Widget is a web page or web application that is embedded as an element of a host web page but which is substantially independent of the host page
It is actually a small web-app inside another page that is embedded and sandboxed inside another page.
Most popular are the Dashlets or dashboard widgets.

Steps:

1.Create a folder /widgets
2.Create folder /widgets/chartjs/ and create files: chartjs.js, chartjs.html & chartjs.css in there
3.To add

```
</script>
// jQuery style selection: $(#main).innerHTML=val
	function $(id,val) {
		document.getElementById(id).innerHTML = val;
			}
	// add a Listener for button1 click event!
	document.getElementById("button1").addEventListener('onclick', function() {
		console.log(“button1 has been clicked);
	})
</script>

```

#The Scada Project

Scada Controlling Things using Socket.io / nodejs / RaspberryPi gpio

Giving commands from your browser via the socket to the RBPi A socket is a connection between 2 points and it works with nodejs.

```
<div id="julie"><>
	 class= "blue"
	 
```

#Map Rpi gpIO Pins Hardware & Software

gpio stands for General

Instead of using a Tagname, class or getElementById("theo").id (or getElementById("blue").class. We use the "$" sign: eg. $('# theo') or $('.blue')

Steps to  install the pigpio:

1.Type: sudo nodeapp.js
2.Type: sudo apt-get install pigpio
3.Type: npm install pigpio

#Project Arduino Lcd Project

1.Display text from an arduino uno to a 16x2 lcd screen
2.Add a sensor to the circuit and display the sensor data
3.Add led to the circuit
4.Add piezo (buzzer)

Using the pins of the RBPi for the leds:
pin 17 for 11
pin 18 for 12
pin 27 for 13
pin 22 for 15

![Screenshot](../img/iap10.jpg)

#Built the circuit

![Screenshot](../img/iap11.jpg)

#Add scada project to new project folder

nodejs

         projects

                 project2

                    scada

npm install express

![Screenshot](../img/iap12.jpg)

npm install socket.io

![Screenshot](../img/iap13.jpg)

sudo apt-get install pigpio

![Screenshot](../img/iap14.jpg)

sudo node app.js

![Screenshot](../img/iap15.jpg)

#The code in app.js

```
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var path = require('path');
// Now setup the local hardware IO
var Gpio = require('pigpio').Gpio;
// start your server
var port=4000;
server.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err)
  }

  console.log('server is listening on port 4000')
})

// routing your client app (stored in the /public folder)
app.use(express.static(path.join(__dirname, 'public')));


// Handling Socket messages  as soon as socket becomes active
io.sockets.on('connection', function (socket) {
    // Hookup button behaviour within socket to submit an update when pressed (TO DO)

    // when the client emits 'opencmd', this listens and executes
    socket.on('opencmd', function (data) {
        // Add calls to IO here
        io.sockets.emit('opencmd', socket.username, data);
        setTimeout(function () {
            io.sockets.emit('openvalve', socket.username, data);
            console.log("user: "+socket.username+" opens LED" + data);
            // add some error handling here
            led = new Gpio(parseInt(data), {mode: Gpio.OUTPUT});
            led.digitalWrite(1);
        }, 1000)
    });

    // when the client emits 'closecmd', this listens and executes
    socket.on('closecmd', function (data) {
        // Add calls to IO here
        io.sockets.emit('closecmd', socket.username, data);
        setTimeout(function () {
            io.sockets.emit('closevalve', socket.username, data);
            console.log("user: "+socket.username+" closes LED" + data);
            // add error handling here
            led = new Gpio(parseInt(data), {mode: Gpio.OUTPUT});
            led.digitalWrite(0);
        }, 1000)

/*      setTimeout(function () {
            led.digitalWrite(0);
            io.sockets.emit('closevalve', socket.username, data);
        }, 1000) */
    });

});

```

#The code in index.html

```
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<meta http-equiv="X-UA-Compatible" content="IE=9" />

<head>
    <title>Codettes Tutorial</title>
    <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
    <link type="text/css" rel="stylesheet" href="css/app.css" media="all">
</head>
<script src="js/socket.js"></script>
<script src="js/jquery.js"></script>
<script>
    var socket = io.connect(window.location.hostname + ":4000"); //'http://192.168.1.163:4000'); //set this to the ip address of your node.js server

    // listener, whenever the server emits 'openvalve', this updates the username list
    socket.on('opencmd', function(username, data) {
        $('#' + data + ' > div.feedback > div.circle.status > div.circle.command').removeClass('red').addClass('green');
    });
    socket.on('openvalve', function(username, data) {
        $('#' + data + ' > div.feedback > div.circle.status').removeClass('red').addClass('green');
    });


    // listener, whenever the server emits 'openvalve', this updates the username list
     socket.on('closecmd', function(username, data) {
        $('#' + data + ' > div.feedback > div.circle.status > div.circle.command').removeClass('green').addClass('red');
    });
    socket.on('closevalve', function(username, data) {
        $('#' + data + ' > div.feedback > div.circle.status').removeClass('green').addClass('red');
    });

    // on load of page
    $(function() {

        // when the client clicks OPEN
        $('.open').click(function() {
            var id = $(this).parent().attr("id");;
            console.log("user clicked open : " + id);
            socket.emit('opencmd', id);
        });

        // when the client clicks CLOSE
        $('.close').click(function() {
            var id = $(this).parent().attr("id");;
            console.log("user clicked on close : " + id);
            socket.emit('closecmd', id);
        });

    });
</script>

<body>
    <p>

        <div id="17" class="valve">
            <h3>Control 1</h3>
            <div class="open">OPEN</div>
            <div class="close">CLOSE</div>
            <div class="clear"></div>
            <div class="feedback">
                <div class="circle status green">
                    <div class="circle command red"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div id="27" class="valve">
            <h3>Control 2</h3>
            <div class="open">OPEN</div>
            <div class="close">CLOSE</div>
            <div class="clear"></div>
            <div class="feedback">
                <div class="circle status green">
                    <div class="circle command red"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div id="18" class="valve">
            <h3>Control 3</h3>
            <div class="open">OPEN</div>
            <div class="close">CLOSE</div>
            <div class="clear"></div>
            <div class="feedback">
                <div class="circle status green">
                    <div class="circle command red"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div id="23" class="valve">
            <h3>Control 4</h3>
            <div class="open">OPEN</div>
            <div class="close">CLOSE</div>
            <div class="clear"></div>
            <div class="feedback">
                <div class="circle status green">
                    <div class="circle command red"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
</body>

```

#Now you can turn on and off your Led's

![Screenshot](../img/iap16.jpg)
