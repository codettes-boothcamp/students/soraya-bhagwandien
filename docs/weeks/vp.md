#Video Production

In week2

We learn how to: 
-Install Adobe Premiere

#Steps of video editting
-Create New Project=New → Project

![Screenshot](../img/screenshot9.jpg)

-Give a name
-Capture Format select for HDV
-Other Default

![Screenshot](../img/screenshot10.jpg)

-Import Video/Img= File → Import

![Screenshot](../img/screenshot11.jpg)
 
-WorkSpace → Project / Timeline / Effects
Drag video (left )to timeline (right)

![Screenshot](../img/screenshot12.jpg)

-Effects → Ultra Key
-Effects Controls → Key Color (remove background

![Screenshot](../img/screenshot13.jpg)

-Import → Background Image
Drag background image to timeline between video and audio

![Screenshot](../img/screenshot14.jpg)

-If background small (step1)
Right click → Scale to Frame Size

![Screenshot](../img/screenshot15.jpg)

-If background small (step2)
Effects Control → Scale 120 or more

![Screenshot](../img/screenshot16.jpg)

-Adding Titles
New → Legacy Title

![Screenshot](../img/screenshot17.jpg)

-Adding Titles
New Title → Name

![Screenshot](../img/screenshot18.jpg)

-Adding Titles (step1)
Select Tool → Text 
Font Family Algeria

![Screenshot](../img/screenshot19.jpg)

-Adding Titles (step2)
Drag title to timeline 
Place / Stretch as pleased

![Screenshot](../img/screenshot20.jpg)

-Adding Transitions
1.Effects → 2.Video Transition  
Select any → Drag onto timeline session

![Screenshot](../img/screenshot21.jpg)

-Exporting 
1.Export → Media

![Screenshot](../img/screenshot22.jpg)

-Exporting 
2.Youtube → H.264

![Screenshot](../img/screenshot23.jpg)

-Exporting 
3.Preset → HD 720p 29.97

![Screenshot](../img/screenshot24.jpg)

-Exporting
4.Preset → Youtube 720p HD

![Screenshot](../img/screenshot25.jpg)

-Exporting9
5.Output → Select Path

![Screenshot](../img/screenshot26.jpg)

-Exporting
6.Scroll to bottom : Export 
(default path : Output)

![Screenshot](../img/screenshot27.jpg)

